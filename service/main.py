import os
import mlflow
from fastapi import FastAPI, Query
from pydantic import BaseModel

from pipeline import TaskExtractor


class TextQuery(BaseModel):
    text: str


mlflow.set_tracking_uri("http://212.113.117.27:5000")
os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://212.113.117.27:9000'
os.environ['MLflow_TRACKING_USERNAME'] = 'MLflow-user'


def get_model_path(model_path):
    mlflow.artifacts.download_artifacts(
        run_id="e116aa4de5d94d0ab363838ec986fece",  # run_id in mlflow
        dst_path=f"{model_path}"
    )
    return model_path


task_extractor = TaskExtractor(get_model_path("../models"))
app = FastAPI()


@app.post("/generate_tasks")
async def generate_tasks(text_query: TextQuery):
    return {"tasks": task_extractor.get_several_tasks(text_query.text)}
